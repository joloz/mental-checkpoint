# Mental Checkpoint
[View site online](https://joloz.gitlab.io/mental-checkpoint/)

Hey there, this is a little exerciese I did about micro sites. And since I felt really inspired by the logo animation of [Mental Checkpoint](https://www.youtube.com/c/MentalCheckpoint) I made it branded like that channel.

## Important legal stuff

This isn't officially approved by the Mental Checkpoint channel owner and is not intended to fake being them, consider it a fan project.
